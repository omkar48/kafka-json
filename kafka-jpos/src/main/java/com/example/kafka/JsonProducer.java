package com.example.kafka;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.example.model.User;

@Service
public class JsonProducer {

	private static final Logger LOGGER = LoggerFactory.getLogger(JsonProducer.class);
	
//	Properties properties = new Properties();
//	properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,JsonSerializer.class.getId());
	
	
	private KafkaTemplate<String, User> kafkaTemplate;
	
	
	public JsonProducer(KafkaTemplate<String, User> kafkaTemplate) {
		super();
		this.kafkaTemplate = kafkaTemplate;
	}
	
	public void sendMsg(User data) {
		
		LOGGER.info(String.format("Message sent -> in json format"  , data));
		
		Message<User> message =MessageBuilder
				.withPayload(data)
				.setHeader(KafkaHeaders.TOPIC,"json_response")
				.build();
		
		kafkaTemplate.send(message);
	}
	
	
	
}
