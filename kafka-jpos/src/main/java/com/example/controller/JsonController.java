package com.example.controller;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.JsonProducer;
import com.example.model.User;

@RestController
@RequestMapping("/api")
public class JsonController {

	@Autowired
	private JsonProducer jsonProducer;

	public JsonController(JsonProducer jsonProducer) {
		super();
		this.jsonProducer = jsonProducer;
	}
	
	@PostMapping("/jsonMsg")
	public ResponseEntity<String> display(@RequestBody User user){
		jsonProducer.sendMsg(user);
		return ResponseEntity.ok("Message sent to topic in json format");
	}
	
	
}
